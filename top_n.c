#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {

	if (argc != 3) {
		printf("Usage: ./top_n <file_name> <N>\n");
		return 1;
	}

	//arguments
	char *file_name = argv[1];
	long int n = atol(argv[2]);

	//variables
	long int *largest_numbers, array_size = 0, current_number, temporary_number, i, j;

	//file helpers
	FILE *fp;
	char *line = NULL;
	size_t len = 0;
	ssize_t read;

	largest_numbers = (long int *) malloc(n * sizeof(long int));

	//file handling
	fp = fopen(file_name, "r");

	while ((read = getline(&line, &len, fp)) != -1) {
		current_number = atol(line);
		if (array_size == n && current_number <= largest_numbers[array_size - 1])
			continue;

		for (i = 0; i < array_size; i++)
			if (current_number > largest_numbers[i])
				for (j = i; j < array_size; j++) {
					temporary_number = largest_numbers[j];
					largest_numbers[j] = current_number;
					current_number = temporary_number;
				}

		if (array_size < n) {
			largest_numbers[array_size] = current_number;
			array_size++;
		}
	}

	fclose(fp);

	printf("The largest %ld numbers are:", array_size);
	for (i = 0; i < array_size; i++)
		printf(" %ld", largest_numbers[i]);
	printf("\n");

	if (array_size < n)
		printf("WARNING: You asked for %ld top numbers, but there are only %ld numbers on the file\n", n, array_size);

	free(largest_numbers);

}